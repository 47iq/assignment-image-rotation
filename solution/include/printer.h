//
// Created by admin on 17.12.2021.
//

#ifndef PRINTER_H
#define PRINTER_H

enum error_code {
    ERROR_INVALID_SIGNATURE,
    ERROR_INVALID_HEADER,
    ERROR_INVALID_BITS ,
    ERROR_NULL_PTR,
    ERROR_WRITE,
    ERROR_CLOSE,
    ERROR_OPEN,
    ERROR_ARGS
};

void print_error(enum error_code code);

#endif //PRINTER_H
