//
// Created by admin on 17.12.2021.
//

#ifndef PADDING_H

#include "stdbool.h"
#include "stdint.h"
#include "stdio.h"

#include "image.h"

uint32_t calculate_padding(uint32_t image_width);

uint32_t padding_size_bytes(struct image const *img);

bool file_write_padding(FILE* const out, uint32_t image_width);

#endif //PADDING_H
