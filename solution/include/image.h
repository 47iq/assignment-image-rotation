//
// Created by admin on 25.09.2021.
//

#ifndef IMAGE_H
#define IMAGE_H
#include <inttypes.h>
#include <stdbool.h>

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image image_create(uint32_t width, uint32_t height);

void image_destroy(struct image *img);

bool image_set_pixel(struct image *img, struct pixel const p, uint32_t x, uint32_t y);

struct pixel image_get_pixel(struct image const* img, uint32_t x, uint32_t y);

uint32_t image_get_size_bytes(struct image const* img);

bool image_cords_valid(struct image const* img);

#endif //IMAGE_H
