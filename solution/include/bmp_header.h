//
// Created by admin on 28.11.2021.
//

#ifndef BMP_HEADER_H
#define BMP_HEADER_H
#include <stdint.h>

#include "image.h"

#define HEADER_BF_TYPE 0x4D42
#define HEADER_OFF_BITS 54
#define HEADER_BI_SIZE 40
#define HEADER_BI_PLANES 1
#define HEADER_BI_BIT_COUNT 24
#define HEADER_BI_X_PPM 2834
#define HEADER_BI_Y_PPM 2834
#define HEADER_CLR_USED 0
#define HEADER_CLR_IMP 0
#define HEADER_BF_RESERVED 0
#define HEADER_BI_COMPRESSION 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header header_from_image(const struct image* img);

bool header_dimensions_valid(const struct bmp_header* header);

bool header_signature_valid(const struct bmp_header* header);

#endif //BMP_HEADER_H
