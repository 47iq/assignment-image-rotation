//
// Created by admin on 28.11.2021.
//

#include <stdbool.h>
#include <stdio.h>

#include "../include/file_manager.h"

bool open_file(FILE **file, char const *name, char const *mode) {
    if (!name)
        return false;
    *file = fopen(name, mode);
    return *file;
}

bool close_file(FILE *file) {
    if (!file)
        return false;
    return !fclose(file);
}
