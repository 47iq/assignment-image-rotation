//
// Created by admin on 17.12.2021.
//

#include "stdio.h"

#include "../include/printer.h"

static const char* error_outputs[] = {
        [ERROR_INVALID_SIGNATURE] = "Файл не является BMP.",
        [ERROR_INVALID_HEADER] = "Невалидный заголовок BMP файла.",
        [ERROR_INVALID_BITS] = "Невалидное изображение в файле.",
        [ERROR_NULL_PTR] = "Непредвиденная ошибка.",
        [ERROR_WRITE] = "Ошибка при записи в файл.",
        [ERROR_CLOSE] = "Ошибка при закрытии файлов.",
        [ERROR_OPEN] = "Ошибка при открытии файлов.",
        [ERROR_ARGS] = "Для запуска необходимы пути до входного и выходного файла в аргументах программы."
};

void print_error(enum error_code code) {
    fprintf(stderr, "%s %s", error_outputs[code], "\n");
}


